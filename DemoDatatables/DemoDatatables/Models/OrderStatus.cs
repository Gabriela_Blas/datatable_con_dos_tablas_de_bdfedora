﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DemoDatatables.Models
{

    [Table("sc_OrderStatus")]
    public class sc_OrderStatus
    {
        [Key]

        public string OrderID { get; set; }
        public string Status { get; set; }
        public string CreateBy { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string Comment { get; set; }
        public System.Guid UID { get; set; }
    }
}