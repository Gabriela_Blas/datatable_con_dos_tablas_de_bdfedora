﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DemoDatatables.Models;
using System.Linq;
using System.Web;


namespace DemoDatatables.Models
{
    [Table ("smm_OrderMaster")]
    public class smm_OrderMaster
    {
    [Key]
        public string OrderID { get; set; }
        public Nullable<System.DateTime> OrderDate { get; set; }
        public string CustomerType { get; set; }
        public string CustomerName { get; set; }
        public string Email { get; set; }
        public string ProductType { get; set; }
        public string ProductModel { get; set; }
        public string ProductDescription { get; set; }
        public string PartNumber { get; set; }
        public string SerialNumber { get; set; }
        public string FailureType { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public string SeqNo { get; set; }
        public bool? inWarranty { get; set; }
        public string DanoEmpaque { get; set; }
        public string DanoEquipo { get; set; }
        public bool? CID { get; set; }
        public bool? Validado { get; set; }
        public int? IntRR { get; set; }
        public int? ContDiario { get; set; }
    }

}