﻿using DemoDatatables.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Linq.Dynamic;
using System.Data.Entity;

using System.Collections.Generic;

namespace DemoDatatables.Controllers
{
    public class DemoController : Controller
    {
        // GET: Demo
        public ActionResult ShowGrid()
        {
            return View();
        }

        public ActionResult LoadData()
        {
            try
            {
                //Creating instance of DatabaseContext class
                using (DatabaseContext _context = new DatabaseContext())
                {
                    var draw = Request.Form.GetValues("draw").FirstOrDefault(); //dibuja secuencia de devoluciones de forma asincrona
                    var start = Request.Form.GetValues("start").FirstOrDefault(); //inicio en el conjunto de datos actual(0 basado en índice, es decir, 0 es el primer registro).
                    var length = Request.Form.GetValues("length").FirstOrDefault();// Número de registros que la tabla puede mostrar
                    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();


                    //Paging Size (10,20,50,100)  
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;

                    // obteniendo todos los datos de la orden 
                    var ordenData = (from temporden in _context.smm_OrderMaster
                                     select temporden);

                    //Sorting (clasificación) 
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        ordenData = ordenData.OrderBy(sortColumn + " " + sortColumnDir);
                    }

                    ////Search  buscar
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        ordenData = ordenData.Where(m => m.OrderID == searchValue
                        || m.CustomerType == searchValue || m.CustomerName == searchValue || m.Email == searchValue || m.ProductType==searchValue );
                    }

                    //total number of rows count   
                    recordsTotal = ordenData.Count();
                    //Paging   
                    var data = ordenData.Skip(skip).Take(pageSize).ToList();

                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data },JsonRequestBehavior.AllowGet);
                    
                }
            }

            catch (Exception)
            {
                
                throw;
            }

        }

        [HttpGet]
        public ActionResult Edit(string ID)
        {
            try
            {
                using (DatabaseContext _context = new DatabaseContext())
                {
                    var Orden = (from smm_OrderMaster in _context.smm_OrderMaster
                                 where smm_OrderMaster.OrderID == ID
                                 select smm_OrderMaster).FirstOrDefault();

                    return View(Orden);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        [HttpPost]
        public JsonResult DeleteOrden(string ID)
        {
            using (DatabaseContext _context = new DatabaseContext())
            {
                var Orden = _context.smm_OrderMaster.Find(ID);

                if (ID == null)
                    return Json(data: "Not Deleted", behavior: JsonRequestBehavior.AllowGet);
                _context.smm_OrderMaster.Remove(Orden);
                _context.SaveChanges();

                return Json(data: "Deleted", behavior: JsonRequestBehavior.AllowGet);
            }
        }

    }
}