﻿using DemoDatatables.Models;
using System;
using System.Linq;
using System.Linq.Dynamic;
using System.Web.Mvc;
using System.Data.Entity;

namespace DemoDatatables.Controllers
{
    public class OrderStatusController : Controller
    {
        // GET: OrderStatus
        public ActionResult showOrderStatus()
        {
            return View();
        }
        public ActionResult LoadOrderStatus()
        {
            try
            {
                //Creating instance of DatabaseContext class
                using (DatabaseContext _context = new DatabaseContext())
                {
                    var draw = Request.Form.GetValues("draw").FirstOrDefault(); //dibuja secuencia de devoluciones de forma asincrona
                    var start = Request.Form.GetValues("start").FirstOrDefault(); //inicio en el conjunto de datos actual(0 basado en índice, es decir, 0 es el primer registro).
                    var length = Request.Form.GetValues("length").FirstOrDefault();// Número de registros que la tabla puede mostrar
                    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();


                    //Paging Size (10,20,50,100)  
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;

                    // obteniendo todos los datos de la orden 
                    var ordenData = (from temporden in _context.sc_OrderStatus
                                     select temporden);

                    //Sorting (clasificación) 
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        ordenData = ordenData.OrderBy(sortColumn + " " + sortColumnDir);
                    }

                    ////Search  buscar
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        ordenData = ordenData.Where(m => m.OrderID == searchValue
                        || m.Status == searchValue || m.Comment == searchValue || m.CreateBy == searchValue);
                    }

                    //total number of rows count   
                    recordsTotal = ordenData.Count();
                    //Paging   
                    var data = ordenData.Skip(skip).Take(pageSize).ToList();

                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);

                }
            }

            catch (Exception)
            {

                throw;
            }

        }

        [HttpGet]
        public ActionResult Edit(string ID)
        {
            try
            {
                using (DatabaseContext _context = new DatabaseContext())
                {
                    var Orden = (from sc_OrderStatus in _context.sc_OrderStatus
                                 where sc_OrderStatus.OrderID == ID
                                 select sc_OrderStatus).FirstOrDefault();

                    return View(Orden);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        [HttpPost]
        public JsonResult DeleteOrden(string ID)
        {
            using (DatabaseContext _context = new DatabaseContext())
            {
                var Orden = _context.sc_OrderStatus.Find(ID);

                if (ID == null)
                    return Json(data: "Not Deleted", behavior: JsonRequestBehavior.AllowGet);
                _context.sc_OrderStatus.Remove(Orden);
                _context.SaveChanges();

                return Json(data: "Deleted", behavior: JsonRequestBehavior.AllowGet);
            }
        }

    }
}
    
